﻿using System;
using System.Diagnostics;

namespace PKOapp
{
    class Program
    {
        private static string MY_APP_ID = "PKOapp";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                var initialParam = args[0].Split('?');
                switch (initialParam[0])
                {
                    case "pkoapp://appstart/":
                        if(initialParam.Length == 2)
                        {
                            StartNewInstance(initialParam[1]);
                        }
                        else
                        {
                            WrongCommand("PKOapp.exe błędne parametry startowe");
                        }                        
                        break;
                    case "pkoapp://appstop/":
                        StopAllInstances();
                        break;
                    default:
                        Console.WriteLine("!" + initialParam[0] + "!");
                        foreach (string txt in args)
                        {
                            Console.WriteLine("|"+txt+"|");
                        }
                        WrongCommand("PKOapp.exe nieznana komenda");
                        break;
                }
            }
            else
            {
                WrongCommand("PKOapp.exe brak parametrów startowych");
            }
        }

        private static void WrongCommand(string errorMessage)
        {
            Console.WriteLine(errorMessage);
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
            Environment.Exit(0);
        }

        private static void StopAllInstances()
        {
            CloseAllOtherInstances();
            CloseMeImmediatelly();
        }

        private static void CloseMeImmediatelly()
        {
            Environment.Exit(0);
        }

        private static void StartNewInstance(string startingParams)
        {
            CloseAllOtherInstances();
            Console.WriteLine("PKOapp.exe uruchomiona z parametrami:\r\n");
            Console.WriteLine(startingParams.Replace('&', '\n'));
            CloseApp();
        }

        private static void CloseAllOtherInstances()
        {
            Process currentProcess = Process.GetCurrentProcess();
            Process[] localByName = Process.GetProcessesByName(MY_APP_ID);
            if (localByName.Length > 1)
            {
                foreach(Process proces in localByName)
                {
                    if(proces.Id != currentProcess.Id)
                    {
                        proces.CloseMainWindow();
                    }
                }
            }
        }
        
        private static void CloseApp()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
            Environment.Exit(0);
        }
    }
}
