# Instalacja: #
## Windows 7/10:##
* na komputerze klienta umieścić plik **PKOapp.exe** domyślna lokalizacja ***c:\pko\PKOapp.exe***
* uruchomić plik **pkoapp.reg** (jesli ścieżka do pliku jest inna niż domyślna, należy pkoapp.reg odpowiednio zmodyfikować)
* uruchomić program **start_pko_app.html** w przeglądarce internetowej. *Rozwiazanie było testowane na przeglądarkach Firefox, Chrome oraz Internet Explorer 11*

## ALTERNATYWNIE: Windows 10 + przeglądarka Firefox ##
* w przeglądarce wpisać about:config nastapi otworzenie strony konfiguracyjnej
* dodać nowy wpis typu boolean: network.protocol-handler.expose.pkoapp o wartosci false
* uruchomić program start_pko_app.html w przeglądarce Firefox. Przy pierwszym kliknieciu w link przeglądarka zapyta sie jakiego programu uzyć do obsłuzenia protokołu. Należy wskazać plik PKOapp.exe